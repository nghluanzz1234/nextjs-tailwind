/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */

// animations:
// animation - [name] : animation - spin ....
// animation - [duration - key]: animation - 2s ....
// animation - [timing - function- key]: animation - ease ....
// animation - delay - [key]: animation - delay - 2s ....
// animation - [iteration - count - key]: animation - once ....
// animation - [direction - key]: animation - reverse....
// animation - fill - [key]: animation - fill - forwards...
// animation - running
// animation - paused

const tailwindAnimationsConfig = {
  animations: { // defaults to {}; the following are examples
    spin: {
      from: {
        transform: 'rotate(0deg)',
      },
      to: {
        transform: 'rotate(360deg)',
      },
    },
    jump: {
      '0%': {
        transform: 'translateY(0%)',
      },
      '50%': {
        transform: 'translateY(-100%)',
      },
      '100%': {
        transform: 'translateY(0%)',
      },
    },
  },
  animationDuration: { // defaults to these values
    default: '1s',
    '0s': '0s',
    '1s': '1s',
    '2s': '2s',
    '3s': '3s',
    '4s': '4s',
    '5s': '5s',
  },
  animationTimingFunction: { // defaults to these values
    default: 'ease',
    linear: 'linear',
    ease: 'ease',
    'ease-in': 'ease-in',
    'ease-out': 'ease-out',
    'ease-in-out': 'ease-in-out',
  },
  animationDelay: { // defaults to these values
    default: '0s',
    '0s': '0s',
    '1s': '1s',
    '2s': '2s',
    '3s': '3s',
    '4s': '4s',
    '5s': '5s',
  },
  animationIterationCount: { // defaults to these values
    default: 'infinite',
    once: '1',
    infinite: 'infinite',
  },
  animationDirection: { // defaults to these values
    default: 'normal',
    normal: 'normal',
    reverse: 'reverse',
    alternate: 'alternate',
    'alternate-reverse': 'alternate-reverse',
  },
  animationFillMode: { // defaults to these values
    default: 'none',
    none: 'none',
    forwards: 'forwards',
    backwards: 'backwards',
    both: 'both',
  },
  animationPlayState: { // defaults to these values
    running: 'running',
    paused: 'paused',
  },
};

module.exports = {
  theme: {
    colors: require('tailwindcss-open-color'),
    opacity: {
      0: '0',
      25: '.25',
      50: '.5',
      75: '.75',
      10: '.1',
      20: '.2',
      30: '.3',
      40: '.4',
      60: '.6',
      70: '.7',
      80: '.8',
      90: '.9',
      100: '1',
    },
    ...tailwindAnimationsConfig,
    extend: {
    },
  },
  variants: [],
  plugins: [
    // can use twind.macro
    require('tailwindcss-animations'),
    require('tailwindcss-elevation')(),
    // cant use twind.macro
    require('tailwind-bootstrap-grid')({
      gridColumns: 24,
      generateContainer: false,
      containerMaxWidths: {
        sm: '540px', md: '720px', lg: '960px', xl: '1140px',
      },
    }),
  ],
};

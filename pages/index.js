import { Tooltip } from 'antd';

function SpaceDemo() {
  return (
    <div className="flex justify-center pt-56">
      <Tooltip title="prompt text" className="rounded-full bg-green-4">
        <span>Tooltip will show on mouse enter.</span>
      </Tooltip>
    </div>

  );
}

export default SpaceDemo;
